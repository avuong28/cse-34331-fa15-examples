// stack_list.cpp: list stack

#include <list>
#include <stack>

const int NITEMS = 1<<25;

int main(int argc, char *argv[])
{
    std::stack<int, std::list<int>> s;

    for (int i = 0; i < NITEMS; i++) {
    	s.push(i);
    }

    while (!s.empty()) {
    	s.pop();
    }

    return 0;
}
