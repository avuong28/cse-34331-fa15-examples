// search_binary.cpp: simple binary search

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

const int NITEMS  = 1<<20;
const int NSEARCH = 1<<10;

int main(int argc, char *argv[]) {
    std::vector<int> haystack;

    srand(time(NULL));

    for (int i = 0; i < NITEMS; i++) {
    	haystack.push_back(rand());
    }

    std::sort(haystack.begin(), haystack.end());

    for (int i = 0; i < NSEARCH; i++) {
	int needle = rand();

	std::cout << "Searching for " << needle;
	if (std::binary_search(haystack.begin(), haystack.end(), needle)) {
	    std::cout << " Yeah!" << std::endl;
	} else {
	    std::cout << " Nope!" << std::endl;
	}
    }

    return 0;
}
