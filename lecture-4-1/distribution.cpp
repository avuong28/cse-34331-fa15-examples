#include <cstdint>
#include <functional>
#include <iostream>
#include <vector>
#include <set>

using namespace std;

#define TABLE_SIZE (10)
#define VALUE_BASE (2)

// http://isthe.com/chongo/tech/comp/fnv/

#define FNV_OFFSET_BASIS    (0xcbf29ce484222325ULL)
#define FNV_PRIME	    (0x100000001b3ULL)

template <typename T>
size_t fnv_hash_value(T const& value) {
    uint64_t hash = FNV_OFFSET_BASIS;

    for (uint8_t i = 0; i < sizeof(T); i++) {
    	uint8_t byte = static_cast<uint8_t>(value >> i*8);

    	hash = hash ^ byte;
    	hash = hash * FNV_PRIME;
    }

    return hash;
}

template <typename T>
size_t nop_hash_value(T const& value) {
    return value;
}

template <typename T>
size_t stl_hash_value(T const& value) {
    hash<T> hasher;
    return hasher(value);
}

template <typename T>
class SCTable {

    typedef size_t (hash_func)(T const&);

public:
    SCTable(int size=TABLE_SIZE, hash_func *func=nop_hash_value<T>) {
    	hfunc = func;
    	tsize = size;
	table = vector<set<T>>(tsize);
    }

    void insert(const T &value) {
    	int bucket = hfunc(value) % tsize;

    	table[bucket].insert(value);
    }

    bool find(const T &value) {
    	int bucket = hfunc(value) % tsize;

    	return table[bucket].count(value);
    }

    void print() const {
    	for (size_t i = 0; i < table.size(); i++) {
    	    cout << i << ":";
    	    for (auto value : table[i]) {
    	    	cout << " " << value;
	    }
    	    cout << endl;
	}
    }

private:
    vector<set<T>> table;
    int tsize;
    hash_func *hfunc;
};

int main(int argc, char *argv[]) {
    SCTable<int> s1;
    SCTable<int> s2(TABLE_SIZE, fnv_hash_value<int>);
    SCTable<int> s3(TABLE_SIZE, stl_hash_value<int>);

    if (argc != 2) {
    	cerr << "usage: " << argv[0] << " nitems" << endl;
    	return 1;
    }

    int nitems = atoi(argv[1]);

    for (int i = 0; i < nitems; i++) {
    	s1.insert(VALUE_BASE + i*TABLE_SIZE);
    	s2.insert(VALUE_BASE + i*TABLE_SIZE);
    	s3.insert(VALUE_BASE + i*TABLE_SIZE);
    }

    cout << "NOP Hash:" << endl;
    s1.print();
    cout << endl;
    
    cout << "FNV1-a Hash:" << endl;
    s2.print();
    cout << endl;
    
    cout << "STL Hash:" << endl;
    s3.print();
    cout << endl;

    return 0;
}
