// selection_sort.cpp

#include <algorithm>
#include <iostream>

using namespace std;

// Selection sort:
//
//  For each slot in the array, starting from the back and moving to the front
//
//	Scan the array from the beginning up until the back
//	    Look for the largest element
//
//	Swap the largest element with the one at the back
void selection_sort(int a[], size_t n) {
    if (n <= 1)
    	return;

    for (int back = n - 1; back > 0; back--) {
    	int largest = 0;

    	for (int i = 1; i <= back; i++)
    	    if (a[i] > a[largest])
    	    	largest = i;

	swap(a[largest], a[back]);

	for_each(a, a + n, [](int i) { cout << i << " "; }); cout << endl;
    }
}

int main(int argc, char *argv[]) {
    int numbers[] = {5, 4, 7, 0, 1};

    cout << "Unsorted:" << endl;
    for_each(numbers, numbers + 5, [](int i) { cout << i << " "; }); cout << endl;

    selection_sort(numbers, 5);

    cout << "Sorted:" << endl;
    for_each(numbers, numbers + 5, [](int i) { cout << i << " "; }); cout << endl;

    return 0;
}
