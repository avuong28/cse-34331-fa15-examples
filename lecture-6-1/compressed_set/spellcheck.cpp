#include <iostream>
#include <fstream>
#include <string>

//#include <set>
#include "compressed_set.hpp"

using namespace std;

int main() {
  ifstream dictfile("/usr/share/dict/words");
  string line;
  compressed_set<string> dict(235886, 1e-2);

  while (getline(dictfile, line)) {
    dict.insert(line);
  }

  while (getline(cin, line)) {
    cout << line << " is ";
    if (dict.count(line) > 0)
      cout << "GOOD";
    else
      cout << "BAD";
    cout << endl;
  }
}
