#ifndef COMPRESSED_SET_HPP
#define COMPRESSED_SET_HPP

#include "SpookyV2.h"

// Alternate hash class that allows a seed.
// (This is defined as a template like std::hash, but only actually
// works for strings.)

// Instantiate it like this:
//   alt_hash<string> hasher;
// Then use it like this:
//   size_t h = hasher("Go Irish");

template <typename T>
struct alt_hash {
  std::size_t operator() (const std::string &s) const = 0;
};

template <>
struct alt_hash<std::string> {
  SpookyHash hasher;
  std::size_t seed;
  alt_hash (std::size_t seed=0) : seed(seed) { }
  std::size_t operator() (const std::string &s) const {
    return hasher.Hash64(s.c_str(), s.size(), seed);
  }
};

template <typename T>
class compressed_set {
public:
  compressed_set(int n, double epsilon) {
      // TODO: Initialize internal class members based on n and epsilon
  }

  std::size_t count(const T &x) const {
      // TODO: Return 1 if x is in set, otherwise return 0
  }

  void insert(const T &x) {
      // TODO: Insert x into set
  }

  std::size_t num_bits() const {
      // TODO: Return number of bits in set
  }

private:
  // TODO:
  //	1. Define internal composite hash function
  //
  //	2. Declare internal class members
};

#endif

