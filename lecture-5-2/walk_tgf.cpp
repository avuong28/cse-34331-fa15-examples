// walk_tgf.cpp: read TGF and walk it

#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

// WalkType Constants

typedef enum {
    DFS_REC,
    DFS_ITER,
    BFS_ITER,
} WalkType;

// Graph structure

template <typename NodeLabel, typename EdgeLabel>
struct Graph {
    vector <NodeLabel> labels;
    vector <map<int, EdgeLabel>> edges;
};

// Load graph from standard input

template <typename NodeLabel, typename EdgeLabel>
void load_graph(Graph<NodeLabel, EdgeLabel> &g) {
    string line;

    // Clear Graph
    g.labels.clear();
    g.edges.clear();

    // Read labels (vertices)
    g.labels.push_back(""); // TGF starts nodes at 1, so add dummy node

    while (getline(cin, line) && line[0] != '#') {
        stringstream ss(line);
        int node;
        string label;

        ss >> node;
        getline(ss, label);
        g.labels.push_back(label);
    }

    // Read edges (vertices)
    g.edges.resize(g.labels.size());

    while (getline(cin, line)) {
        stringstream ss(line);
        int source;
        int target;
        string label;

        ss >> source >> target;
        getline(ss, label);

        g.edges[source][target] = label;
    }
}

// Walk graph dispatch function

template <typename NodeLabel, typename EdgeLabel>
void walk_graph(Graph<NodeLabel, EdgeLabel> &g, size_t root, WalkType w) {
    vector<bool> visited(g.labels.size());

    switch (w) {
        case DFS_REC:
            walk_graph_dfs_rec(g, root, visited);
            break;
        case DFS_ITER:
            walk_graph_dfs_iter(g, root, visited);
            break;
        case BFS_ITER:
            walk_graph_bfs_iter(g, root, visited);
            break;
        default:
            cerr << "Unknown WalkType: " << w << endl;
            break;
    }
}

// Depth-First-Search (recursive)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_rec(Graph<NodeLabel, EdgeLabel> &g, size_t v, vector<bool> &visited) {
    // If already visited, stop processing
    if (visited[v]) {
        return;
    }

    // Visit by printing out vertex and associated label
    cout << "Visit Vertex " << v << ": " << g.labels[v] << endl;

    // Mark as visited
    visited[v] = true;

    // Recurse on edges
    for (auto &edge : g.edges[v]) {
        walk_graph_dfs_rec(g, edge.first, visited);
    }
}

// Depth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_dfs_iter(Graph<NodeLabel, EdgeLabel> &g, size_t v, vector<bool> &visited) {
    stack<size_t> frontier;
    frontier.push(v);

    while (!frontier.empty()) {
        v = frontier.top();
        frontier.pop();

        if (visited[v]) {
            continue;
        }

        cout << "Visit Vertex " << v << ": " << g.labels[v] << endl;

        visited[v] = true;

        for (auto edge = g.edges[v].rbegin(); edge != g.edges[v].rend(); edge++) {
            frontier.push(edge->first);
        }
    }
}

// Breadth-First-Search (iterative)

template <typename NodeLabel, typename EdgeLabel>
void walk_graph_bfs_iter(Graph<NodeLabel, EdgeLabel> &g, size_t v, vector<bool> &visited) {
    queue<size_t> frontier;
    frontier.push(v);

    while (!frontier.empty()){
        v = frontier.front();
        frontier.pop();

        if (visited[v]) {
            continue;
        }

        cout << "Visit Vertex " << v << ": " << g.labels[v] << endl;

        visited[v] = true;

        for(auto &u: g.edges[v]) {
            frontier.push(u.first);
        }
    }
}

// Main execution

int main(int argc, char *argv[]) {
    Graph<string, string> g;

    if (argc != 3) {
        cerr << "usage: " << argv[0] << " root [0 = DFS_REC | 1 = DFS_ITER | 2 = BFS_ITER]" << endl;
        return EXIT_FAILURE;
    }

    load_graph(g);
    walk_graph(g, static_cast<size_t>(atoi(argv[1])), static_cast<WalkType>(atoi(argv[2])));

    return EXIT_SUCCESS;
};
