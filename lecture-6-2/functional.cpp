#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    vector<int> data {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    // Goal: determine the sum of all the even squares of elements in data
    int total = 0;
    for (auto number: data) {
    	int square = number*number;
    	if (square % 2 == 0)
    	    total += square;
    }

    cout << total << endl;

    // Now functionally
    auto dbegin = data.begin();
    auto dend   = data.end();

    transform(dbegin, dend, dbegin, [](const int x){ return x*x; });
    dend   = remove_if(dbegin, dend, [](const int x){ return x % 2 != 0; });
    total  = accumulate(dbegin, dend, 0, plus<int>());

    cout << total << endl;
    return 0;
}
