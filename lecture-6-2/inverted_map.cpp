#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char *argv[]) {
    string line;

    while (getline(cin, line)) {
    	string key;
    	string value;

    	stringstream ss(line);
    	ss >> key;

    	key = key.substr(0, key.find(":"));

    	while (ss >> value) {
    	    if (value.size() == 0)
    	    	continue;

    	    size_t paren = value.find("(");
    	    if (paren == string::npos)
    	    	continue;

	    value = value.substr(0, paren);
	    cout << value << "\t" << key << endl;
	}
    }

    return 0;
}
