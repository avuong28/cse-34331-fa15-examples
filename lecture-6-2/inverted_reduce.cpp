#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <set>

using namespace std;

int main(int argc, char *argv[]) {
    string line;
    string key;
    string value;
    string prev_key = "";
    set<string> prev_files;

    while (getline(cin, line)) {
    	stringstream ss(line);
    	ss >> key >> value;

    	if (key == prev_key) {
    	    prev_files.insert(value);
	} else {
	    if (prev_key != "") {
	    	cout << prev_key << "\t";
	    	for_each(prev_files.begin(), prev_files.end(), [](const string &s){ cout << " " << s; });
	    	cout << endl;
	    }

	    prev_key = key;
	    prev_files.clear();
	    prev_files.insert(value);
	}
    }

    cout << prev_key << "\t";
    for_each(prev_files.begin(), prev_files.end(), [](const string &s){ cout << " " << s; });
    cout << endl;

    return 0;
}
