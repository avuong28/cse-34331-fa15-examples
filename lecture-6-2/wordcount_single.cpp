#include <iostream>
#include <sstream>
#include <unordered_map>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    string line;
    unordered_map<string, int> count;

    while (getline(cin, line)) {
    	string word;

    	stringstream ss(line);

    	while (ss >> word) {
	  count[word] += 1;
	}
    }
    for (auto wc : count) {
      cout << wc.first << "\t" << wc.second << "\n";
    }
    return 0;
}
