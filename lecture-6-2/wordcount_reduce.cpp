#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[]) {
    string line;
    string key;
    int    value;
    string prev_key = "";
    int    prev_count = 0;

    while (getline(cin, line)) {
    	stringstream ss(line);
    	ss >> key >> value;

    	if (key == prev_key) {
    	    prev_count += value;
	} else {
	    if (prev_key != "") {
	    	cout << prev_key << "\t" << prev_count << endl;
	    }

	    prev_key   = key;
	    prev_count = value;
	}
    }

    cout << prev_key << "\t" << prev_count << endl;

    return 0;
}
