#include <chrono>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <list>

using namespace std;

int main(int argc, char *argv[]) {
    chrono::time_point<chrono::system_clock> start, end;
    list<int> v1;
    list<int> v2;
    list<int> v3;

    for (int i = 0; i < 1<<21; i++) {
    	v1.push_back(rand());
    	v2.push_back(rand());
    }

    v1.sort();
    v2.sort();

    start = chrono::system_clock::now();
    set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), back_inserter(v3));
    end   = chrono::system_clock::now();

    chrono::duration<double> elapsed_seconds = end-start;
    cout << elapsed_seconds.count() << endl;

    return 0;
}
